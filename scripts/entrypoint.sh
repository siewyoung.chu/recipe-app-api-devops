#!/bin/sh

set -e

python manage.py collectstatic --noinput
python manage.py wait_for_db
python manage.py migrate

# Run uWSGI as a TCP socket on port 9000
# Run this as the master service on the terminal
# Enable multi threading
# Module is the application that uWSGi is going to run
uwsgi --socket :9000 --workers 4 --master --enable-threads --module app.wsgi